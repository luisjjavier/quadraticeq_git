import sys
import getopt
import math
def main () :
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["help"])
    except getopt.error, msg:
        print msg
        print "Usa -h --help o /h para ayuda"
        sys.exit(2)
    for o,a in opts:
        if o in ("-h", "--help","/h"):
            print "Se debe ejecutar quadraticEq 2 -4 para una ecuacion en formato (2x^2 - 4x)"
            print "Se debe ejecutar quadraticEq 1 2 3  para una ecuacion en formato (x^2 + 2x + 3)"
            sys.exit(0)
    if len(args) == 0 :
        print "Se debe ejecutar quadraticEq 2 -4 para una ecuacion en formato (2x^2 - 4x)"
        print "Se debe ejecutar quadraticEq 1 2 3  para una ecuacion en formato (x^2 + 2x + 3)"
        sys.exit(0)
    for arg in args :
        if arg.isdigit() == False:
            print "Uno de los argumentos pasados no cumplen con el contrato de entrada"
            sys.exit(0)
    a= int(args[0])
    b =  0 if len(args) <=1 else int(args[1])
    c =  0 if len(args) <=2 else int(args[2])
    d = b**2-4*a*c 
   #print c,d,a,b,len(args)
    if d < 0:
        print 'No hay solucion real'
    elif d == 0:
        x1 = -b / (2*a)
        print 'La unica solucion es ',x1
    else: 
        x1 = (-b + math.sqrt(d)) / (2*a)
        x2 = (-b - math.sqrt(d)) / (2*a)
        print 'Las soluciones son  X1:',x1,'X2:',x2
    sys.exit(0)
if __name__ == "__main__":
    main()
